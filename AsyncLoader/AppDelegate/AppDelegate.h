//
//  AppDelegate.h
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/14/16.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

