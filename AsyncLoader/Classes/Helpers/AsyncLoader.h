//
//  AsyncLoader.h
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/14/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AsyncLoader : NSObject

+(AsyncLoader *)sharedLoader;
+(NSCache *)defaultCache;

/**
 Maximum concurrent files that can be downloaded simultaneously. Smaller value will reasult into faster downloads of items while larger number may enable to download more items with effect on speed.
 */
@property (nonatomic, assign) NSUInteger concurrentLoads;

/**
 Cache Size in MB, example maxCacheSize = 5 would mean 5 MB
 Default is 10
 Note : limits are imprecise/not strict
 */
@property (nonatomic, assign) NSUInteger maxCacheSize;

/**
 Specify async request timeout in seconds. Default is 60 sec
 */
@property (nonatomic, assign) NSTimeInterval loadingTimeout;

-(void)loadItemWithURL:(NSURL *)URL target:(id)target success:(SEL)success failure:(SEL)failure;
-(void)cancelLoadingItemsForTarget:(id)target action:(SEL)action;

-(void)clearCache;
-(NSUInteger)getCacheSize;
@end
