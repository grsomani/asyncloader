//
//  AsyncLoader.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/14/16.
//
//

#import "AsyncLoader.h"
#import <objc/message.h>
#import <pthread.h>

NSString *const AsyncLoadDidFinish = @"AsyncLoadDidFinish";
NSString *const AsyncLoadDidFail = @"AsyncLoadDidFail";

NSString *const AsyncItemKey = @"AsyncItem";
NSString *const AsyncItemMimeKey = @"AsyncMimeItem";
NSString *const AsyncItemURLKey = @"AsyncItemURL";
NSString *const AsyncCacheKey = @"AsyncCache";
NSString *const AsyncErrorKey = @"AsyncError";

@interface AsyncItemConnection : NSObject

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *itemData;
@property (nonatomic, strong) NSString *itemMIME;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) id target;
@property (nonatomic, assign) SEL success;
@property (nonatomic, assign) SEL failure;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (nonatomic, getter = isCancelled) BOOL cancelled;

-(AsyncItemConnection *)initWithURL:(NSURL *)URL
                                cache:(NSCache *)cache
                               target:(id)target
                              success:(SEL)success
                              failure:(SEL)failure;

-(void)start;
-(void)cancel;
-(BOOL)isInCache;

@end


@implementation AsyncItemConnection

-(AsyncItemConnection *)initWithURL:(NSURL *)URL
                                cache:(NSCache *)cache
                               target:(id)target
                              success:(SEL)success
                              failure:(SEL)failure
{
    if ((self = [self init]))
    {
        self.URL = URL;
        self.cache = cache;
        self.target = target;
        self.success = success;
        self.failure = failure;
    }
    return self;
}

-(id)cachedItem
{
    if([self.cache objectForKey:self.URL])
    {
        //Exist in cache
        return [self.cache objectForKey:self.URL];
    }
    else if ([self.URL isFileURL])
    {
        NSString *path = [[self.URL absoluteURL] path];
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        if ([path hasPrefix:resourcePath])
        {
            return [NSData dataWithContentsOfFile:[path substringFromIndex:[resourcePath length]]];;
        }
    }
    return nil;
}

-(BOOL)isInCache
{
    return [self cachedItem] != nil;
}

-(void)loadFailedWithError:(NSError *)error
{
    self.loading = NO;
    self.cancelled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:AsyncLoadDidFail
                                                        object:self.target
                                                      userInfo:@{AsyncItemURLKey: self.URL,
                                                                 AsyncErrorKey: error}];
}

-(void)cacheItem:(id)item
{
    if (!self.cancelled)
    {
        if (item && self.URL)
        {
            BOOL storeInCache = YES;
            if ([self.URL isFileURL])
            {
                if ([[[self.URL absoluteURL] path] hasPrefix:[[NSBundle mainBundle] resourcePath]])
                {
                    //File in bundle, do not store in cache
                    storeInCache = NO;
                }
            }
            if (storeInCache)
            {
                [self.cache setObject:item forKey:self.URL];
                [self.cache setObject:self.itemMIME forKey:[NSString stringWithFormat:@"Mime:%@", self.URL]];
            }
        }
        
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         item, AsyncItemKey,
                                         self.itemMIME, AsyncItemMimeKey,
                                         self.URL, AsyncItemURLKey,
                                         nil];
        if (self.cache)
        {
            userInfo[AsyncCacheKey] = self.cache;
        }
        
        self.loading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:AsyncLoadDidFinish
                                                            object:self.target
                                                          userInfo:[userInfo copy]];
    }
    else
    {
        self.loading = NO;
        self.cancelled = NO;
    }
}

-(void)connection:(__unused NSURLConnection *)connection didReceiveResponse:(__unused NSURLResponse *)response
{
    self.itemData = [NSMutableData data];
    self.itemMIME = [response MIMEType];
}

-(void)connection:(__unused NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //add data
    [self.itemData appendData:data];
}

-(void)connectionDidFinishLoading:(__unused NSURLConnection *)connection
{
    if(self.itemData)
    {
        [self performSelectorOnMainThread:@selector(cacheItem:)
                               withObject:self.itemData
                            waitUntilDone:YES];
    }
    self.connection = nil;
    self.itemData = nil;
}

-(void)connection:(__unused NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.connection = nil;
    self.itemData = nil;
    [self loadFailedWithError:error];
}

-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

-(void)connection:(NSURLConnection *) connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge
{
    if (([challenge.protectionSpace.authenticationMethod
          isEqualToString:NSURLAuthenticationMethodServerTrust]))
    {
        NSURLCredential *credential = [NSURLCredential credentialForTrust:
                                       challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:credential
             forAuthenticationChallenge:challenge];
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)start
{
    if (self.loading && !self.cancelled)
    {
        return;
    }
    
    //begin loading
    self.loading = YES;
    self.cancelled = NO;
    
    //check for nil URL
    if (self.URL == nil)
    {
        [self cacheItem:nil];
        return;
    }
    
    //check for cached item
    id item = [self cachedItem];
    if (item)
    {
        //add to cache (cached already but it doesn't matter)
        [self performSelectorOnMainThread:@selector(cacheItem:)
                               withObject:item
                            waitUntilDone:NO];
        return;
    }
    
    //begin load
    NSURLRequest *request = [NSURLRequest requestWithURL:self.URL
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:[AsyncLoader sharedLoader].loadingTimeout];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    [self.connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    [self.connection start];
}

- (void)cancel
{
    self.cancelled = YES;
    [self.connection cancel];
    self.connection = nil;
    self.itemData = nil;
}

@end

@interface AsyncLoader()
{
    pthread_mutex_t mutex;
}

@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) NSMutableArray *connections;
@property (nonatomic, strong) dispatch_semaphore_t cacheSizeSemaphore;
@end

@implementation AsyncLoader

+(AsyncLoader *)sharedLoader
{
    static AsyncLoader *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [(AsyncLoader *)[self alloc] init];
    }
    return sharedInstance;
}

+(NSCache *)defaultCache
{
    static NSCache *sharedCache = nil;
    if (sharedCache == nil)
    {
        sharedCache = [[NSCache alloc] init];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(__unused NSNotification *note) {
            
            [sharedCache removeAllObjects];
        }];
    }
    return sharedCache;
}

-(AsyncLoader *)init
{
    if ((self = [super init]))
    {
        self.cache = [[self class] defaultCache];
        _concurrentLoads = 10;
        _loadingTimeout = 60.0;
        _maxCacheSize = 10;
        [self.cache setTotalCostLimit:10 * 1024 *1024];
        _connections = [[NSMutableArray alloc] init];
        pthread_mutex_init(&mutex, NULL);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(itemLoaded:)
                                                     name:AsyncLoadDidFinish
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(itemLoadingFailed:)
                                                     name:AsyncLoadDidFail
                                                   object:nil];
    }
    return self;
}

-(void)loadItemWithURL:(NSURL *)URL target:(id)target success:(SEL)success failure:(SEL)failure
{
    //check cache
    __block id item = [self.cache objectForKey:URL];
    if (item)
    {
        [self cancelLoadingItemsForTarget:self action:success];
        if (success)
        {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                
                NSString *itemMIMEData = (NSString *)[self.cache objectForKey:[NSString stringWithFormat:@"Mime:%@",URL]];
                ((void (*)(id, SEL, id, id, id))objc_msgSend)(target, success, item, itemMIMEData, URL);
            });
        }
        return;
    }
    
    
    //create new connection
    AsyncItemConnection *connection = [[AsyncItemConnection alloc] initWithURL:URL
                                                                           cache:self.cache
                                                                          target:target
                                                                         success:success
                                                                         failure:failure];
    BOOL added = NO;
    for (NSUInteger i = 0; i < [self.connections count]; i++)
    {
        AsyncItemConnection *existingConnection = self.connections[i];
        if (!existingConnection.loading)
        {
            [self.connections insertObject:connection atIndex:i];
            added = YES;
            break;
        }
    }
    if (!added)
    {
        [self.connections addObject:connection];
    }
    
    [self updateQueue];
}

-(void)cancelLoadingItemsForTarget:(id)target action:(SEL)action
{
    for (NSInteger i = (NSInteger)[self.connections count] - 1; i >= 0; i--)
    {
        AsyncItemConnection *connection = self.connections[(NSUInteger)i];
        if (connection.target == target && connection.success == action)
        {
            [connection cancel];
        }
    }
}

-(void)updateQueue
{
    //start connections
    NSUInteger count = 0;
    for (AsyncItemConnection *connection in self.connections)
    {
        if (![connection isLoading])
        {
            if ([connection isInCache])
            {
                [connection start];
            }
            else if (count < self.concurrentLoads)
            {
                count ++;
                [connection start];
            }
        }
    }
}

-(void) setMaxCacheSize:(NSUInteger)maxCacheSize
{
    _maxCacheSize = 10;
    [[AsyncLoader sharedLoader].cache setTotalCostLimit:maxCacheSize * 1024 *1024];
}

-(void) clearCache
{
    [[AsyncLoader sharedLoader].cache removeAllObjects];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    dispatch_queue_t q = dispatch_queue_create("com.fun.asyncloader.clearCache", NULL);
    
    //Lock So that getCacheSize doesnt get incorrect size while cache is being cleared
    pthread_mutex_lock(&mutex);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    basePath = [basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",[[NSBundle mainBundle] bundleIdentifier]]];
    
    dispatch_async(q, ^{
        [fileManager removeItemAtPath:basePath error:nil];
        [fileManager createDirectoryAtPath:basePath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:NULL];
        
        //Signal clearCache Completion
        pthread_mutex_unlock(&mutex);
    });
    
}

-(NSUInteger)getCacheSize
{
    __block NSUInteger size = 0;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    dispatch_queue_t q = dispatch_queue_create("com.fun.asyncloader.getCacheSize", NULL);
    dispatch_sync(q, ^{
        
        //Wait if cache is being clear
        pthread_mutex_lock(&mutex);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        basePath = [basePath stringByAppendingString:[NSString stringWithFormat:@"/%@",[[NSBundle mainBundle] bundleIdentifier]]];
        
        NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:basePath];
        for (NSString *fileName in fileEnumerator) {
            NSError *attributeserror = nil;
            NSString *filePath = [basePath stringByAppendingPathComponent:fileName];
            NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&attributeserror];
            size += [attrs fileSize];
        }
        pthread_mutex_unlock(&mutex);
    });
    return size;
}

#pragma mark - Loading Finished Notifications
-(void)itemLoaded:(NSNotification *)notification
{
    //complete connections for URL
    NSURL *URL = (notification.userInfo)[AsyncItemURLKey];
    for (NSInteger i = (NSInteger)[self.connections count] - 1; i >= 0; i--)
    {
        AsyncItemConnection *connection = self.connections[(NSUInteger)i];
        if (connection.URL == URL || [connection.URL isEqual:URL])
        {
            //cancel earlier connections for same target/action
            for (NSInteger j = i - 1; j >= 0; j--)
            {
                AsyncItemConnection *earlier = self.connections[(NSUInteger)j];
                if (earlier.target == connection.target &&
                    earlier.success == connection.success)
                {
                    [earlier cancel];
                    [self.connections removeObjectAtIndex:(NSUInteger)j];
                    i--;
                }
            }
            
            //cancel connection (in case it's a duplicate)
            [connection cancel];
            
            //perform action
            id item = (notification.userInfo)[AsyncItemKey];
            id itemMIMEData = (notification.userInfo)[AsyncItemMimeKey];
            if(connection.success)
            {
                ((void (*)(id, SEL, id, id, id))objc_msgSend)(connection.target, connection.success, item, itemMIMEData, connection.URL);
            }
            
            //remove from queue
            [self.connections removeObjectAtIndex:(NSUInteger)i];
        }
    }
    
    //update the queue
    [self updateQueue];
}

-(void)itemLoadingFailed:(NSNotification *)notification
{
    //remove connections for URL
    NSURL *URL = (notification.userInfo)[AsyncItemURLKey];
    for (NSInteger i = (NSInteger)[self.connections count] - 1; i >= 0; i--)
    {
        AsyncItemConnection *connection = self.connections[(NSUInteger)i];
        if ([connection.URL isEqual:URL])
        {
            //cancel connection (in case it's a duplicate)
            [connection cancel];
            
            //perform failure action
            if (connection.failure)
            {
                NSError *error = (notification.userInfo)[AsyncErrorKey];
                ((void (*)(id, SEL, id, id))objc_msgSend)(connection.target, connection.failure, error, URL);
            }
            
            //remove from queue
            [self.connections removeObjectAtIndex:(NSUInteger)i];
        }
    }
    
    //update the queue
    [self updateQueue];
}
@end
