//
//  AsyncLoaderDemoViewController.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//

#import "AsyncLoader.h"
#import "AsyncLoaderDemoViewController.h"

#define Image_Item_URL "http://media.treehugger.com/assets/images/2016/04/tracy-johnson-hummingbirds-1.jpg.662x0_q70_crop-scale.jpg"
#define JSON_Item_URL "http://mysafeinfo.com/api/data?list=englishmonarchs&format=json"
#define XML_Item_URL "http://www.w3schools.com/xml/note.xml"

@interface AsyncLoaderDemoViewController ()

@property (weak, nonatomic) IBOutlet UITextField *itemURLTextField;
@property (weak, nonatomic) IBOutlet UILabel *requestCacheStatusLabel;
@end

@implementation AsyncLoaderDemoViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Demo";
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction functions
- (IBAction)imageItemButtonPressed:(id)sender
{
    self.itemURLTextField.text = [NSString stringWithFormat:@"%s", Image_Item_URL];
}

- (IBAction)jsonItemButtonPressed:(id)sender
{
    self.itemURLTextField.text = [NSString stringWithFormat:@"%s", JSON_Item_URL];
}

- (IBAction)xmlItemButtonPressed:(id)sender
{
    self.itemURLTextField.text = [NSString stringWithFormat:@"%s", XML_Item_URL];
}

- (IBAction)requestItemButtonPressed:(id)sender
{
    NSString *urlString = [self.itemURLTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([urlString isEqualToString:@""])
    {
        [self presentErrorMessage:@"Please enter a URL"];
        return;
    }
    
    NSURL *candidateURL = [NSURL URLWithString:urlString];
    if(candidateURL && candidateURL.scheme && candidateURL.host)
    {
        if([[AsyncLoader defaultCache] objectForKey:[NSURL URLWithString:self.itemURLTextField.text]])
        {
            NSString *itemMimeData = [[AsyncLoader defaultCache] objectForKey:[NSString stringWithFormat:@"Mime:%@",[NSURL URLWithString:self.itemURLTextField.text]]];
            self.requestCacheStatusLabel.text = [NSString stringWithFormat:@"Cache Hit : %@",itemMimeData];
        }
        else
        {
            self.requestCacheStatusLabel.text = @"Cache Miss";
            [[AsyncLoader sharedLoader] loadItemWithURL:[NSURL URLWithString:self.itemURLTextField.text] target:self success:@selector(didFetchItemData:mimeType:) failure:@selector(didFailToFetchItemData:)];
        }
    }
    else
    {
        [self presentErrorMessage:@"Incorrect URL. Please enter a valid URL"];
        return;
    }
}

#pragma mark - AsyncLoader Handling Methods
-(void) didFetchItemData:(id) data mimeType:(NSString *)mimeData
{
    self.requestCacheStatusLabel.text = [NSString stringWithFormat:@"Cache Miss : %@", mimeData];
}

-(void) didFailToFetchItemData:(NSError *) errorData
{
    self.requestCacheStatusLabel.text = errorData.localizedDescription;
}

#pragma mark - Input / Error Handling
-(void) presentErrorMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

}
@end
