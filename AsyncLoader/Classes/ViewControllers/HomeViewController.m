//
//  ViewController.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/14/16.
//
//

#import "HomeViewController.h"

#import "PDKClient.h"
#import "PDKPin.h"
#import "PDKResponseObject.h"
#import "PDKUser.h"

#import "PinsViewController.h"
#import "AsyncLoaderDemoViewController.h"

@interface HomeViewController ()

@property (nonatomic, strong) PDKUser *user;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewPinsButtons;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Sample";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Handling
-(void)updateButtonEnabledState
{
    [self.viewPinsButtons setEnabled:YES];
}

#pragma mark - Actions
-(IBAction)authenticatePinterest:(id)sender
{
    __weak HomeViewController *weakSelf = self;
    [[PDKClient sharedInstance] authenticateWithPermissions:@[PDKClientReadPublicPermissions,
                                                              PDKClientWritePublicPermissions,
                                                              PDKClientReadRelationshipsPermissions,
                                                              PDKClientWriteRelationshipsPermissions]
                                                withSuccess:^(PDKResponseObject *responseObject)
     {
         weakSelf.user = [responseObject user];
         weakSelf.resultLabel.text = [NSString stringWithFormat:@"%@ authenticated!", weakSelf.user.firstName];
         [weakSelf updateButtonEnabledState];
     } andFailure:^(NSError *error) {
         weakSelf.resultLabel.text = @"authentication failed";
     }];
}

-(IBAction)viewPinsButtonPressed:(id)sender
{
    PinsViewController *pinsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PinsViewControllerID"];
    [self.navigationController pushViewController:pinsVC animated:YES];
}

- (IBAction)moreFunctionButtonPressed:(id)sender
{
    AsyncLoaderDemoViewController *asyncLoadedDemoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AsyncLoaderDemoID"];
    [self.navigationController pushViewController:asyncLoadedDemoVC animated:YES];
}
@end
