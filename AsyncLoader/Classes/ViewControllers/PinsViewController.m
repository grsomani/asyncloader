//
//  PinsViewController.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//

#import "PinsViewController.h"
#import "PDKClient.h"
#import "PDKResponseObject.h"
#import "PDKPin.h"
#import "PinCollectionViewCell.h"

@interface PinsViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *pinsCollectionView;
@property (nonatomic, strong) NSArray *pins;
@property (nonatomic, strong) PDKResponseObject *lastResponseObject;
@property (nonatomic, assign) BOOL fetchingMore;

@end

@implementation PinsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"All Pins";
    [self.pinsCollectionView registerNib:[UINib nibWithNibName:@"PinCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PinCell"];
    [self fetchPins];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) fetchPins
{
    __weak PinsViewController *weakSelf = self;
    [[PDKClient sharedInstance] getAuthenticatedUserPinsWithFields:[NSSet setWithArray:@[@"id", @"image", @"note", @"url"]]
                                                           success:^(PDKResponseObject *responseObject)
    {
        self.lastResponseObject = responseObject;
        weakSelf.pins = [responseObject pins];
        [weakSelf.pinsCollectionView reloadData];
    }
                                                        andFailure:^(NSError *error)
    {
        NSLog(@"Pins fetch failed");
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.pins count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PDKPin *pin = self.pins[indexPath.row];
    PinCollectionViewCell *cell = (PinCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"PinCell" forIndexPath:indexPath];
    [cell setPinItem:pin];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PDKPin *pin = self.pins[indexPath.row];
    [[UIApplication sharedApplication] openURL:pin.url];
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.fetchingMore == NO && [self.lastResponseObject hasNext] && self.pins.count - indexPath.row < 5) {
        self.fetchingMore = YES;
        
        __weak PinsViewController *weakSelf = self;
        [self.lastResponseObject loadNextWithSuccess:^(PDKResponseObject *responseObject) {
            weakSelf.fetchingMore = NO;
            weakSelf.lastResponseObject = responseObject;
            weakSelf.pins = [weakSelf.pins arrayByAddingObjectsFromArray:[responseObject pins]];
            [weakSelf.pinsCollectionView reloadData];
        } andFailure:nil];
    }
}
@end
