//
//  PinCollectionViewCell.h
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//

#import <UIKit/UIKit.h>
#import "PDKPin.h"

@interface PinCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) PDKPin *pinItem;

@end
