//
//  PinCollectionViewCell.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//

#import "PinCollectionViewCell.h"
#import "AsyncLoader.h"
#import "PDKImageInfo.h"

@interface PinCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *pinImageView;
@property (weak, nonatomic) IBOutlet UILabel *pinDescriptionLabel;

@end

@implementation PinCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) dealloc
{
    [[AsyncLoader sharedLoader] cancelLoadingItemsForTarget:self action:nil];
}

-(void) setPinItem:(PDKPin *)pinItem
{
    _pinItem = pinItem;
    [self setupPinDetails];
}

-(void) setupPinDetails
{
    //remove Old image as cells are reused
    self.pinImageView.image = nil;
    
    //Cancel Earlier load for the cell
    [[AsyncLoader sharedLoader] cancelLoadingItemsForTarget:self action:nil];
    
    //Start New Load Request
    [[AsyncLoader sharedLoader] loadItemWithURL:self.pinItem.largestImage.url target:self success:@selector(loadPinSuccess:mimeData:andItemURL:) failure:@selector(loadPinFailure:)];
    
    self.pinDescriptionLabel.text = self.pinItem.descriptionText;
}

#pragma mark - Item Load Handling methods
-(void) loadPinSuccess:(id) pinData mimeData:(NSString *) mimeType andItemURL:(NSURL *) imgURL
{
    //re-check if data for current item URL
    if([imgURL.absoluteString isEqualToString:self.pinItem.largestImage.url.absoluteString])
    {
        UIImage *pinImg = [UIImage imageWithData:pinData];
        if(pinImg)
        {
            self.pinImageView.image = pinImg;
        }
    }
}

-(void) loadPinFailure:(id) errorData
{
    
}
@end
