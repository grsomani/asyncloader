//
//  AsyncLoaderTests.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/14/16.
//
//

#import <XCTest/XCTest.h>
#import "AsyncLoader.h"

@interface NSObject (AsyncLoader)

@property (nonatomic, strong) NSMutableArray *connections;

@end

@interface AsyncLoaderTests : XCTestCase

@property (nonatomic) AsyncLoader *asyncLoaderToTest;

@end

@implementation AsyncLoaderTests

- (void)setUp
{
    [super setUp];
    self.asyncLoaderToTest = [AsyncLoader sharedLoader];
}

- (void)tearDown
{
    self.asyncLoaderToTest = nil;
    [super tearDown];
}

-(void) testIfInit
{
    BOOL initSuccess = NO;
    if(self.asyncLoaderToTest.concurrentLoads == 10 && self.asyncLoaderToTest.loadingTimeout == 60 && self.asyncLoaderToTest.maxCacheSize == 10 && [self.asyncLoaderToTest.connections count] == 0)
    {
        initSuccess = YES;
    }
    XCTAssertTrue(initSuccess, @"Init failed");
}

-(void)testClearCache
{
    [self.asyncLoaderToTest clearCache];
    XCTAssertEqualObjects([NSNumber numberWithUnsignedLong:[self.asyncLoaderToTest getCacheSize]], [NSNumber numberWithInt:0], @"Failed to clear cache");
}


-(void)testPerformanceClearCache
{
    [self measureBlock:^{
        [self.asyncLoaderToTest clearCache];
    }];
}

- (void)testPerformanceCalculateCacheSize
{
    [self measureBlock:^{
        [self.asyncLoaderToTest getCacheSize];
    }];
}
@end
