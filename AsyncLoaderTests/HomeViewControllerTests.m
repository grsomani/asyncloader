//
//  HomeViewControllerTests.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//

#import <XCTest/XCTest.h>
#import "HomeViewController.h"
#import "PDKUser.h"

@interface UIViewController (HomeViewController)

@property (nonatomic, strong) PDKUser *user;

@end

@interface HomeViewControllerTests : XCTestCase

@property (nonatomic) HomeViewController *homeVCtoTest;

@end

@implementation HomeViewControllerTests

- (void)setUp
{
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    self.homeVCtoTest = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewID"];

}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testIfUserIsNil
{
    XCTAssertNil(self.homeVCtoTest.user, @"User initialized garbage value");
}

@end
