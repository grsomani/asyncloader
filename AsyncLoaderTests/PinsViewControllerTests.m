//
//  PinsViewControllerTests.m
//  AsyncLoader
//
//  Created by Ganesh Somani on 7/15/16.
//
//


#import <XCTest/XCTest.h>
#import "PinsViewController.h"

@interface UIViewController (PinsViewController)

@property (weak, nonatomic) IBOutlet UICollectionView *pinsCollectionView;
@property (nonatomic, strong) NSArray *pins;

@end

@interface PinsViewControllerTests : XCTestCase

@property (nonatomic, strong) PinsViewController *pinsVC;

@end

@implementation PinsViewControllerTests

- (void)setUp
{
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    self.pinsVC = [storyboard instantiateViewControllerWithIdentifier:@"PinsViewControllerID"];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testPinsArray
{
    XCTAssertNil(self.pinsVC.pins, @"Pins array contains garbage");
}
@end
