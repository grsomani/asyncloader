**Purpose**

AsyncLoader is meant to asynchronously download and cache all kinds of files from the internet. It will return the loaded data in the NSData format along with the MIME type so that appropriate action can taken on the obtained data.

**Supported OS & SDK Versions**

Supported build target - iOS 9.3 (Xcode 7.3, Apple LLVM compiler 7.1)
Earliest supported deployment target - iOS 8.0

**Installation**

To use the AsyncLoader in an app, just drag the AsyncLoader class files into your project.


**Usage**


```
#!objective-c

@property (nonatomic, assign) NSUInteger concurrentLoads;

```
decides the maximum files that can be loaded simultaneously


```
#!objective-c

@property (nonatomic, assign) NSUInteger maxCacheSize;

```
Cache Size in MB, Note :  limits are imprecise/not strict


```
#!objective-c

@property (nonatomic, assign) NSTimeInterval loadingTimeout;

```
Specify async request timeout in seconds.


```
#!objective-c

-(void)loadItemWithURL:(NSURL *)URL target:(id)target success:(SEL)success failure:(SEL)failure;

```
loads the item using the URL and will call back success or failure method at the target provided


```
#!objective-c

-(void)cancelLoadingItemsForTarget:(id)target action:(SEL)action;

```
Cancel loading for specific target.


```
#!objective-c

-(NSUInteger)getCacheSize;

```
returns the size occupied by cache in bytes


```
#!objective-c

-(void) clearCache;

```
Removes loaded items from cache and clears the cache

**Credits**

Code is inspired from AsyncImageView developed by Nick Lockwood - https://github.com/nicklockwood/AsyncImageView